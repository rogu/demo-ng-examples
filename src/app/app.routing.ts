import { RouterModule, Routes } from '@angular/router';
import { ComponentExample } from './examples/component';
import { DirectivesNgExamples } from './examples/directives';
import { UserEventsExamples } from './examples/user-events';
import { DomModyficationExample } from './examples/dom-modyfication';
import { ServiceExample } from './examples/service';
import { PipesExample } from './examples/pipes';
import { HttpExample } from './examples/http';
import { FormModelDrivenExample } from './examples/form-model-driven';
import { RoutesExample, routesExampleRoutes } from './examples/routes';
import { UploadComponentExample } from './examples/upload';
import { InitComponent } from './init.component';
import { LifeCycleComponent, lifecycleRoutes } from './examples/component-lifecycle-hooks';
import { FormTemplateDrivenExample } from './examples/form-template-driven';

const routes: Routes = [
    { path: 'init', component: InitComponent },
    { path: 'component', component: ComponentExample },
    { path: 'directives', component: DirectivesNgExamples },
    { path: 'user-events', component: UserEventsExamples },
    { path: 'dom-modyfication', component: DomModyficationExample },
    { path: 'service', component: ServiceExample },
    { path: 'pipes', component: PipesExample },
    { path: 'http', component: HttpExample },
    { path: 'form-model-driven', component: FormModelDrivenExample },
    { path: 'form-template-driven', component: FormTemplateDrivenExample },
    { path: 'routes', component: RoutesExample, children: routesExampleRoutes },
    { path: 'upload', component: UploadComponentExample },
    { path: 'lifecycle', component: LifeCycleComponent, children: lifecycleRoutes },
    { path: '**', redirectTo: 'init' }
];

export const routingModule = RouterModule.forRoot(routes);
