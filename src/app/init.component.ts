import {Component} from '@angular/core';

@Component({
    template: `
        <h1>Szkolenie Angular | podstawy</h1>
        <p>
            W projekcie tym znajdują się podstawowe zagadnienia związane z Angularem.
            <br>
            Nie jest to przykład aplikacji internetowej lecz prezentacja wybranych mechanizmów Angularowych.
            <br>
            Aby dobrze je zrozumieć zobacz przykłady a następnie zagłębij się w kod.
        </p>
    `
})
export class InitComponent {

}
