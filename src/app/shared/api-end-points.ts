export class API {
    static END_POINT_PREFIX = 'https://api.debugger.pl/';
    static ITEMS_END_POINT = API.END_POINT_PREFIX + 'items';
    static UPLOAD_END_POINT = API.END_POINT_PREFIX + 'utils/upload';
    static IMAGE_END_POINT = 'https://api.debugger.pl/assets/admin.png';

    static AUTH_BASE_END_POINT = 'https://auth.debugger.pl/';
    static LOGIN_END_POINT = API.AUTH_BASE_END_POINT + 'login';
    static IS_AUTHENTICATED = API.AUTH_BASE_END_POINT + 'logged';
}
