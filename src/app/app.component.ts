import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styles: [`
        nav a {
            box-shadow: inset 0 0 1px .5px;
            margin: 5px 5px 0 0;
        }
    `]
})
export class AppComponent {

}
