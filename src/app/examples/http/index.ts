import { Component } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { API } from "src/app/shared/api-end-points";
import { AuthService } from "../routes/services/auth-service";

@Component({
    templateUrl: 'http.component.html',
    styles: [`
        .list-group{
            display:inline-block;
        }
    `]
})

export class HttpExample {

    items: any[];
    access: boolean = false;
    updating: boolean = false;

    constructor(private http: HttpClient, public authService: AuthService) { }

    update(item, price) {
        this.updating = true;
        this.http
            .put(`${API.ITEMS_END_POINT}/${item.id}`, { ...item, price }, {
                withCredentials: true,
                headers: { authorization: this.authService.isLogged }
            })
            .subscribe(resp => this.updating = false);
    }

    signIn() {
        this.authService.signIn('admin@localhost', 'Admin1')
    }

    // Implement this interface to execute custom initialization logic after your directive's data-bound properties have been initialized.
    ngOnInit() {
        this.http
            .get<{ data }>(API.ITEMS_END_POINT)
            .subscribe(({ data }) => this.items = data);
    }

}
