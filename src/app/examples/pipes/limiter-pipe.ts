import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
    name: "limiter"
})
export class LimiterPipe implements PipeTransform {
    transform(arr, num) {
        return arr.slice(0, num);
    }
}
