import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
    name: "search"
})
export class SearchPipe implements PipeTransform {
    transform(arr: any[], text) {
        if(text) {
            return arr.filter((item)=>{
                return item.includes(text);
            })
        } else {
            return arr;
        }
    }
}
