
import {Component} from "@angular/core";

@Component({
    templateUrl: "./pipes.html",
})
export class PipesExample {
    user = {
        name: "Joe",
        phone: 1234234
    };
    items: Array<string> = ['js', 'angular', 'html', 'css', 'jquery'];
    today: number = Date.now();
}
