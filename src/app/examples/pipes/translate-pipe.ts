import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
    name: "translate"
})
export class TranslatePipe implements PipeTransform {
    dict: Object = {
        pl: {
            welcome: "dzień dobry"
        },
        de: {
            welcome: "guten morgen"
        }
    };

    transform(value, lang) {
        let result;
        if (this.dict[lang] && this.dict[lang][value]) {
            result = this.dict[lang][value];
        }
        return result || value;
    }
}
