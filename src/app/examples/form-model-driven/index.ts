import {Component} from "@angular/core";

@Component({
    template: `
        <h6>
        Reactive (Model) forms differ from template-driven forms in distinct ways.
        <br>
        This approach makes our forms testable without a DOM being required and have more validation options.
        <br>
        We build it by creating model in TS file first, then (based on the model) we build template.
        </h6>
        <form-with-formgroup></form-with-formgroup>
        `
})

export class FormModelDrivenExample {

}
