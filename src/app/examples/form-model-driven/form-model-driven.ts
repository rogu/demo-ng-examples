import { Component, OnInit } from "@angular/core";
import { CustomValidators } from "./custom-validators";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Observable, timer } from "rxjs";
import { switchMapTo, map } from "rxjs/operators";
import { API } from "../../shared/api-end-points";
import { HttpClient } from "@angular/common/http";

@Component({
    selector: 'form-with-formgroup',
    templateUrl: "./form-model-driven.html",
    styles: [`
        .badge {margin: 1px}
    `]
})
export class FormWithFormGroup implements OnInit {
    myForm: FormGroup;

    constructor(private http: HttpClient) {

    }

    /**
     * Server valdation - try joe@doe.com
     */
    checkEmail(control): Observable<any> {
        return timer(500)
            .pipe(
                switchMapTo(
                    this.http
                        .get(API.END_POINT_PREFIX + 'exists', { params: { username: control.value } })
                        .pipe(
                            map((resp: { ok, error }) => {
                                if (!resp.ok) {
                                    return { error: resp.error };
                                }
                            })
                        )
                )
            )
    }

    ngOnInit() {
        this.myForm = new FormGroup({
            sku: new FormControl('', Validators.compose([
                Validators.required,
                CustomValidators.skuValidator
            ])),
            nip: new FormControl('', CustomValidators.nipValidator),
            email: new FormControl('', Validators.email, this.checkEmail.bind(this)),
            gender: new FormControl('', Validators.required),
            interests: new FormGroup({
                sport: new FormControl(''),
                music: new FormControl(''),
                technology: new FormControl('')
            }, CustomValidators.atLeastOneIsRequired),
            language: new FormControl('', Validators.required)
        });

        this.myForm
            .valueChanges
            .subscribe(changes => console.log('changes', changes))
    }

    onSubmit(form): void {
        if (form.valid) {
            alert('you submitted value: ' + JSON.stringify(form.value));
        } else {
            console.log('form invalid');
        }
    }
}
