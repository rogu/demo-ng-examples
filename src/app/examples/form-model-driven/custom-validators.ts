import {ValidationErrors} from "@angular/forms";

export class CustomValidators {

    static skuValidator(control: any): ValidationErrors {
        if (!control.value.match(/^123/)) {
            return {
                invalidSku: {
                    msg: 'must start with 123'
                }
            };
        }
    }

    static nipValidator(control: any): ValidationErrors {
        if (!/^[0-9]{10}$/.test(control.value)) {
            return {
                invalidNip: {
                    msg: 'nip should have 10 digital'
                }
            };
        }
    }

    static atLeastOneIsRequired(group: any): ValidationErrors {
        for (const key in group.controls) {
            if (group.controls[key].value) {
                return;
            }
        }
        return {
            atLeastOneIsRequiredInvalid: {
                msg: 'at least one is required'
            }
        };
    }

}
