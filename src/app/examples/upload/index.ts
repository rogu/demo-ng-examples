import { Component } from "@angular/core";
import { API } from "../../shared/api-end-points";

@Component({
    template: `
        <h3>File Upload - max 2MB</h3>
        <file-upload
                [url]="'${API.UPLOAD_END_POINT}'"
                (uploaded)="uploaded($event)">
        </file-upload>

        <img *ngIf="imgSrc" [src]="imgSrc" width="200">
        `
})

export class UploadComponentExample {
    imgSrc: string;

    uploaded(imgSrc) {
        this.imgSrc = imgSrc;
    }
}
