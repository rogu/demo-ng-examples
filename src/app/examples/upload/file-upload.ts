import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'file-upload',
    template: `
        <div>
            Select file:
            <input type="file" (change)="changeHandler($event)">
            <br>
            <b [hidden]="!progress">{{progress}} %</b>
        </div>`
})

export class FileUpload {

    progress: number;
    maxSize: number = 2000000;

    @Input() url: string;
    @Output() uploaded = new EventEmitter();

    changeHandler(evt): void {
        let formData: FormData = new FormData();
        let files = evt.target.files;

        if (files.length === 0) {
            return;
        }

        let file: File = files[0];
        if (file.size > this.maxSize) {
            alert('file is too big');
            return;
        }
        formData.append("file", file, file.name);

        let xhr = new XMLHttpRequest();

        xhr.upload.addEventListener("progress", function (evt: ProgressEvent) {
            this.progress = ((evt.loaded / evt.total) * 100).toFixed();
        }.bind(this), false);

        xhr.addEventListener('load', () => {
            let result = JSON.parse(xhr.response);
            this.uploaded.emit(result.imgSrc);
        });

        xhr.open('POST', this.url, true);
        xhr.send(formData);
    }
}
