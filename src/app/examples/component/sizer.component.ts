import {Component, Input, Output, EventEmitter} from "@angular/core";

@Component({
    selector: "my-sizer",
    host: {
        style: "display: block",
        class: "alert alert-info"
    },
    template: `
        <div>
            <button class="btn btn-success mr-1" (click)="dec()" title="smaller">-</button>
            <button class="btn btn-success" (click)="inc()" title="bigger">+</button>
            <span [style.font-size.px]="size">FontSize(min 8; max 20): {{size}}px</span>
        </div>
    `
})

export class SizerComponent {
    @Input() size: number | string;
    @Output() sizeChange = new EventEmitter<number>();

    dec() {
        this.resize(-1);
    }

    inc() {
        this.resize(+1);
    }

    resize(delta: number) {
        this.size = Math.min(20, Math.max(8, +this.size + delta));
        this.sizeChange.emit(+this.size);
    }
}
