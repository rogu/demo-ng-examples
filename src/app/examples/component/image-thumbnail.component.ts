import {Component, Input, Output, EventEmitter} from "@angular/core";

@Component({
    selector: "img-thumbnail",
    host: {
        class: "d-inline-block mr-3"
    },
    template: `
        <div class="border p-2 m-1">
            <ng-content></ng-content>
            <img [src]="url" width="50" (click)="onClick()">
        </div>
    `
})

export class ImgThumbnailComponent {
    @Input() url;
    @Output() callEvent = new EventEmitter();

    onClick(){
        this.callEvent.emit(this.url);
    }
}
