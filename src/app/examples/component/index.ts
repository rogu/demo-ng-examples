import { Component } from "@angular/core";

@Component({
  template: `
        <h6>
        Component - decorator that marks a class as an Angular component. Includes template, selector and more.
        <br>
        Instance of component is create in HTML by adding selector (HTML tag)
        eg. <![CDATA[<any-component>this is content</any-component> ]]>
        </h6>
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-header">component with input and output & content</div>
                    <div class="card-body">
                        <img-thumbnail [url]="url"
                                     *ngFor="let url of images; let idx = index"
                                     (callEvent)="onEvent($event)">
                            <div>image {{idx + 1}}</div>
                        </img-thumbnail>
                        <hr>
                        <img [src]="bigImage">
                    </div>
                </div>
            </div>

            <div class="col-6">
                <div class="card">
                    <div class="card-header">component with two way data binding</div>
                    <div class="card-body">
                        <div class="alert border">{{size}}</div>
                        <hr>
                        <my-sizer [(size)]="size"></my-sizer>
                    </div>
                </div>
            </div>
        </div>
    `
})

export class ComponentExample {

  bigImage: number;
  size: number = 14;
  images = [
    'https://api.debugger.pl/assets/tomato.jpg',
    'https://api.debugger.pl/assets/pumpkin.jpg',
    'https://api.debugger.pl/assets/potatoes.jpg'
  ];

  constructor() { }

  onEvent(image) {
    this.bigImage = image;
  }
}
