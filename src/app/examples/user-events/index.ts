import { Component } from "@angular/core";

@Component({
    templateUrl: 'user-events.html'
})
export class UserEventsExamples {

    mousePosition: {};

    move(evt) {
        this.mousePosition = { x: evt.clientX, y: evt.clientY };
    }

    action(val) {
        alert('your message is: ' + val);
    }
}
