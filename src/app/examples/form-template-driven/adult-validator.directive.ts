import { Directive, forwardRef } from "@angular/core";
import { Validator, FormControl, NG_VALIDATORS } from "@angular/forms";

export const ADULT_VALIDATOR = {
    provide: NG_VALIDATORS,
    multi: true,
    useExisting: forwardRef(() => AdultValidator)
};

@Directive({
    selector: '[adultValidator]',
    providers: [ADULT_VALIDATOR]
})
export class AdultValidator implements Validator {

    validate(ctrl: FormControl): { [key: string]: any } {
        const valid = ctrl.value && parseInt(ctrl.value) && parseInt(ctrl.value) >= 18;

        return valid ? null : { adultValidator: "invalid" };
    }

}
