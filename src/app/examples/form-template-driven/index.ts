import {Component} from "@angular/core";

@Component({
    template: `
        <h6>
        Angular comes with three different ways of building forms in our applications. Template Form, Reactive Form and Dynamic Form (advanced).
        <br>
        Template-driven approach allows us to build forms with very little to none application code required.
        <br>
        We build the form in the template.
        </h6>
        <form-template-driven></form-template-driven>
        `
})

export class FormTemplateDrivenExample {

}
