import { Injectable } from "@angular/core";

@Injectable({ providedIn: 'root' })
export class MyService {

  currentTime: string = Date.now().toString();

  updateTime() {
    this.currentTime = Date.now().toString();
  }

}
