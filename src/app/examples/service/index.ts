import { Component } from "@angular/core";
import { MyService } from "./my-service";

@Component({
    template: `
        <h6>Services are a class which instance are created by Dependency injection.
            We use them eg for data storage, helpers or to communicate with the server.</h6>
        <div class="card">
            <div class="card-header">Time from service</div>
            <div class="card-body">
                <div class="badge badge-info p-3 mr-2">{{myService.currentTime | date:"H:mm:ss"}}</div>
                <button class="btn btn-success" (click)="myService.updateTime()">update time</button>
            </div>
        </div>
    `
})
export class ServiceExample {
    constructor(public myService: MyService) {

    }
}
