import {Injectable} from "@angular/core";
import {CanActivate} from "@angular/router";
import {AuthService} from "./auth-service";

@Injectable()
export class Guard implements CanActivate {
    constructor(private authService: AuthService) {

    }

    canActivate(): boolean {
        if (this.authService.isLogged) {
            return true;
        } else {
            alert('You shall not pass. \nThis place is protected by a guard in routing. \nTry login. ');
        }
    }
}
