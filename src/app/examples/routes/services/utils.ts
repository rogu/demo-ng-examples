import {Injectable} from "@angular/core";
import {Router} from "@angular/router";

@Injectable()
export class Utils {
    currentRoute: any;

    constructor(private router: Router) {
        router.events.subscribe(evt => {
            this.currentRoute = evt['url'];
        });
    }

    setActive(link) {
        if (this.currentRoute && link && this.currentRoute.indexOf(link) !== -1) {
            return true;
        }
    }
}