import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { API } from "src/app/shared/api-end-points";

@Injectable({ providedIn: 'root' })
export class AuthService {
    accessToken: any;
    message: any;

    constructor(private http: HttpClient) { }

    signIn(username: string, password: string) {
        return this.http
            .post<{ message, accessToken }>(API.LOGIN_END_POINT, { username, password }, { withCredentials: true })
            .subscribe(({ message, accessToken }) => {
                this.message = message;
                this.accessToken = accessToken;
                localStorage.setItem('accessToken', accessToken);
            })
    }

    signOut() {
        localStorage.removeItem('accessToken');
    }

    get isLogged() {
        return localStorage.getItem('accessToken');
    }
}
