import { Component } from '@angular/core';
import { Routes } from '@angular/router';
import { HomeComponent } from './components/home-component';
import { AboutComponent } from './components/about-component';
import { ProductsComponent, productsRoutes as childRoutes } from "./components/products-component";
import { ProtectedComponent } from "./components/protected-component";
import { Guard } from "./services/guard";

export const routesExampleRoutes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'about', component: AboutComponent },
    { path: 'products', component: ProductsComponent, children: childRoutes },
    { path: 'protected', component: ProtectedComponent, canActivate: [Guard] },
];

@Component({
    template: `
        <div>
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link" [routerLinkActive]="['active']" [routerLink]="['home']">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" [routerLinkActive]="['active']" [routerLink]="['about']">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" [routerLinkActive]="['active']" [routerLink]="['products']">Products - deep routing</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" [routerLinkActive]="['active']" [routerLink]="['protected']">Protected</a>
                </li>
                <li><login></login></li>
            </ul>
            <br>
            <router-outlet></router-outlet>
        </div>`
})
export class RoutesExample {

}
