import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    template: `
        <h1>About</h1>
        <a class="btn btn-primary" (click)="gotoHome()">back home</a>
    `
})
export class AboutComponent {

    constructor(private router: Router) { }

    gotoHome(): void {
        this.router.navigate(['routes/home']);
    }
}
