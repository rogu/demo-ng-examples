import { Component } from "@angular/core";
import { Router, Routes } from "@angular/router";
import { FirstProductComponent } from "./products/first.component";
import { SecondProductComponent } from "./products/second.component";
import { ByIdProductComponent } from "./products/byId.component";
import { Utils } from "../services/utils";

export const productsRoutes: Routes = [
    { path: '', redirectTo: 'first', pathMatch: 'full' },
    { path: "first", component: FirstProductComponent },
    { path: 'second', component: SecondProductComponent },
    { path: ':id', component: ByIdProductComponent }
];

@Component({
    providers: [Utils],
    template: `
        <div class="row">
            <div class="col-3">
                <nav class="list-group">
                    <a class="list-group-item" routerLinkActive="active" routerLink="first">first</a>
                    <a class="list-group-item" routerLinkActive="active" routerLink="second">second</a>
                    <a class="list-group-item" [class.active]="helpers.setActive(id.value)">
                        by ID: <input class="form-control" #id (input)="gotoProduct(id.value)">
                    </a>
                </nav>
            </div>
            <div class="col-9">
                <div class="jumbotron">
                    <router-outlet></router-outlet>
                </div>
            </div>
        </div>
    `
})

export class ProductsComponent {

    constructor(private router: Router, public helpers: Utils) { }

    gotoProduct(id) {
        this.router.navigate(['/routes/products', id]);
    }
}
