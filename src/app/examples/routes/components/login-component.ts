import {Component} from "@angular/core";
import {AuthService} from "../services/auth-service";

@Component({
    selector: "login",
    template: `
        <div class="ml-1">
            <form class="form-inline" *ngIf="!authService.isLogged">
                <input #login value="admin@localhost" class="form-control">
                <input type="password" #password value="Admin1" class="form-control">
                <button class="btn btn-success" (click)="authService.signIn(login.value, password.value)">
                    sign in
                </button>
                <span class="badge ml-2"
                    [ngClass]="{'badge-success':authService.isLogged, 'badge-danger':!authService.isLogged}">
                    logged: {{authService.isLogged || 'no'}}
                </span>
            </form>
            <button class="btn btn-warning" *ngIf="authService.isLogged"
                    (click)="authService.signOut()">
                sign out
            </button>
        </div>
    `
})

export class LoginComponent {
    constructor(public authService: AuthService) {

    }
}
