import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  template: `You selected product: {{ id }}`
})
export class ByIdProductComponent {
  id: string;

  constructor(route: ActivatedRoute) {
    route.params.subscribe(({ id }) => this.id = id);
  }
}
