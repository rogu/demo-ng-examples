import {Component} from '@angular/core';

@Component({
    selector: 'protected',
    template: `<h1>Welcome in secure place!</h1>`
})

export class ProtectedComponent {
}
