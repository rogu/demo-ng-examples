import { Component } from '@angular/core';

@Component({
    template: `
        <h4>
            OnChange
        </h4>
        <div>
            The OnChanges hook is called after one or more of our component properties/attrs have been changed.
            The ngOnChanges method receives a parameter which tells which properties have changed
        </div>
        <hr>
        Type something in input.
        <div>
            <label>Name</label>
            <input type="text"
                   autofocus
                   [(ngModel)]="name">
        </div>
        <on-change [name]="name"></on-change>
    `
})
export class OnChangesExample {
    name: string = 'Joe Doe';
}
