import { Component } from '@angular/core';

@Component({
    template: `
            <div>
                <ol>
                    <li>Content is what is passed to component as children: <![CDATA[<any-component>this is content</any-component> ]]></li>
                    <li>View is the template of the current component</li>
                </ol>
                <hr>
                <div class="alert alert-info">
                    <button class="btn btn-success" (click)="toggleAfters()">
                        Refresh
                    </button>
                    <afters *ngIf="displayAfters">my component content</afters>
                </div>
            </div>
    `
})
export class AfterViewInitExample {
    displayAfters: boolean;

    constructor() {
        this.displayAfters = true;
    }

    toggleAfters(): void {
        this.displayAfters = false;
        setTimeout(() => this.displayAfters = true, 100);
    }
}
