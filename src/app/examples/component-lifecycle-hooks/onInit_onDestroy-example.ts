import {
    Component,
    OnInit,
    OnDestroy
} from '@angular/core';

@Component({
    template: `
        <h4>
            OnInit and OnDestroy
        </h4>
        <p>ngOnInit is a life cycle hook called by Angular to indicate that that Angular is done creating the
            component.
            <br>
            OnDestroy is called just before Angular destroys the directive/component.
            This is moment when you can eg. remove listeners/intervals.
        </p>

        <button class="btn btn-primary" (click)="toggle()">
            show/hide
        </button>
        <hr>
        <toggle *ngIf="display"></toggle>
    `
})
export class OnInitExample {
    display: boolean = true;

    toggle(): void {
        this.display = !this.display;
    }
}
