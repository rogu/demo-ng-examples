import { Component } from "@angular/core";
import { Routes } from '@angular/router';
import { OnInitExample } from "./onInit_onDestroy-example";
import { OnChangesExample } from "./onChanges-example";
import { DoCheckIterableExample } from "./doCheck-iterable-example";
import { AfterViewInitExample } from "./viewInit_contentInit-example";
import { DoCheckObjectExample } from "./doCheck-object-example";

@Component({
    templateUrl: 'lifecycle.component.html'
})

export class LifeCycleComponent {

}

export const lifecycleRoutes: Routes = [
    { path: "", redirectTo: "on-init", pathMatch: "full" },
    { path: "on-init", component: OnInitExample },
    { path: "on-changes", component: OnChangesExample },
    { path: "do-check-iterable", component: DoCheckIterableExample },
    { path: "do-check-object", component: DoCheckObjectExample },
    { path: "after-view-init", component: AfterViewInitExample }
];
