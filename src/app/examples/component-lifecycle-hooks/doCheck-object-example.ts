import { Component } from '@angular/core';

@Component({
    template: `
        <h4>
            DoCheck
        </h4>
        <div>
            ngDoCheck gets called to check the changes in the directives in addition to the default algorithm.
            <br>
            The default change detection algorithm looks for differences by comparing bound-property values
            by reference across change detection runs.
        </div>
        <hr>
        <div>you can see effects in browser console</div>
        <div>do click green btn</div>
        <app-do-check-object [comment]="comment"></app-do-check-object>
    `
})
export class DoCheckObjectExample {
    comment = { author: 'Joe', comment: 'tralala', likes: 12 };
}
