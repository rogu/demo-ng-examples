import { Input, OnChanges, Component, SimpleChanges } from "@angular/core";
import { API } from "src/app/shared/api-end-points";

@Component({
    selector: 'on-change',
    template: `
        <div class="alert alert-success">
            <img width="50" src="${API.IMAGE_END_POINT}">
            {{currentName}}
            <br>
            {{lastModification}}
        </div>
    `
})
export class OnChangeComponent implements OnChanges {
    @Input('name') name;
    currentName;
    lastModification;

    ngOnChanges({ name: { currentValue } }: SimpleChanges): void {
        this.currentName = currentValue;
        this.lastModification = `last modification: ${Date.now()}`;
    }
}
