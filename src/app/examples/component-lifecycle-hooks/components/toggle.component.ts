import { OnInit, OnDestroy, Component } from "@angular/core";

@Component({
    selector: 'toggle',
    template: `
        <div class="alert alert-success">
            show/hide component
            <br>
            {{time}}
            <br>
            open chrome console
        </div>
    `
})
export class ToggleComponent implements OnInit, OnDestroy {

    time;
    interval;

    ngOnInit(): void {
        this.interval = setInterval(() => {
            this.time = Date.now();
            console.log(this.time);
        }, 500);
    }

    ngOnDestroy(): void {
        console.log('On destroy');
        clearInterval(this.interval);
    }
}
