import { DoCheck, Input, Output, EventEmitter, KeyValueDiffers, Component } from "@angular/core";

@Component({
    selector: 'app-do-check-object',
    template: `
        <div class="alert alert-info">
            {{comment.author}} say: {{comment.comment}}
            <hr>
            <button class="btn btn-success" (click)="like()">add like ({{comment.likes}})</button>
        </div>
    `
})
export class DoCheckObjectComponent implements DoCheck {
    @Input() comment: any;
    @Output() onRemove: EventEmitter<any> = new EventEmitter();
    differ: any;

    constructor(differs: KeyValueDiffers) {
        this.differ = differs.find([]).create();
    }

    ngDoCheck(): void {
        const changes = this.differ.diff(this.comment);

        if (changes) {
            changes.forEachAddedItem(r => this.logChange('added', r));
            changes.forEachChangedItem(r => this.logChange('changed', r));
        }
    }

    logChange(action, r) {
        if (action === 'changed') {
            console.log('item:', r.key, action, 'from', r.previousValue, 'to', r.currentValue);
        }
        if (action === 'added') {
            console.log('item:', action, r.key, 'with', r.currentValue);
        }
    }

    like(): void {
        this.comment.likes += 1;
    }
}
