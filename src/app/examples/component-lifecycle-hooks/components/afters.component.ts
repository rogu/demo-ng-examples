import { AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, Component } from "@angular/core";

@Component({
    selector: 'afters',
    template: `
        <div class="alert alert-danger">
            <button class="btn btn-primary" (click)="inc()">
                click to see which are running when changing - chrome console: <span class="badge badge-danger">{{ counter }}</span>
            </button>
            <hr>
            <ng-content></ng-content>
        </div>
    `
})
export class AftersComponent implements AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked {

    counter: number = 1;

    inc() {
        this.counter += 1;
    }

    ngAfterContentInit() {
        console.log('AfterContentInit');
    }

    ngAfterViewInit() {
        console.log('AfterViewInit');
    }

    ngAfterContentChecked() {
        console.log('AfterContentChecked');
    }

    ngAfterViewChecked() {
        console.log('AfterViewChecked');

    }
}
