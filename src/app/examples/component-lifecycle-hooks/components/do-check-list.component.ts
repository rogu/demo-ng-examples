import { IterableDiffers, DoCheck, Component } from "@angular/core";

@Component({
    selector: 'do-check-list',
    template: `
        <div class="row">
            <div class="col-sm-2">
                <button class="btn btn-primary btn-sm" (click)="addComment()">
                    Add
                </button>
            </div>
            <div class="col-sm-10">
                <div class="card card-body d-inline-block" *ngFor="let comment of comments">
                    {{comment.author}}, {{comment.comment}}, {{comment.likes}}
                    <button class="btn btn-danger btn-sm" (click)="removeComment(comment)">remove</button>
                </div>
            </div>
        </div>
    `
})
export class DoCheckList implements DoCheck {
    comments: any[];
    authors: string[];
    index: number;
    differ: any;

    constructor(differs: IterableDiffers) {
        this.differ = differs.find([]).create(null);
        this.comments = [];
        this.authors = ['Elliot', 'Helen', 'Jenny'];
        this.index = 0;
        this.addComment();
    }

    addComment(): void {
        this.comments.push({
            author: this.authors[this.comments.length % this.authors.length],
            comment: Date.now(),
            likes: +(Math.random() * 100).toFixed()
        });
    }

    removeComment(comment) {
        let idx = this.comments.indexOf(comment);
        this.comments.splice(idx, 1);
    }

    ngDoCheck(): void {
        let changes = this.differ.diff(this.comments);

        if (changes) {
            changes.forEachAddedItem(r => console.log('list:', 'Added', r.item));
            changes.forEachRemovedItem(r => console.log('list:', 'Removed', r.item));
        }
    }
}
