import { Directive, Input, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
    selector: '[myColor]'
})
export class MyColorDirective {

    @Input() myColor;

    constructor(private el: ElementRef,
                private renderer: Renderer2) {
    }

    @HostListener('mouseenter') onMouseEnter() {
        this.setBorder(this.myColor);
    }

    @HostListener('mouseleave') onMouseLeave() {
        this.setBorder('white');
    }

    setBorder(color) {
        color == null ? this.renderer.removeStyle(this.el.nativeElement, 'backgroundColor') : this.renderer.setStyle(this.el.nativeElement, 'backgroundColor', color);
    }

}
