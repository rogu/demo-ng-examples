import { Component } from "@angular/core";
import { interval } from "rxjs";
import { map } from "rxjs/operators";

@Component({
    template: `
        <h6>
    An Attribute directive changes the appearance or behavior of a DOM element.
</h6>

<div class="row mb-4">
<div class="col-4">
    <div class="card">
        <div class="card-header">*ngIf</div>
        <div class="card-body">
            <div *ngIf="access">you have access</div>
            <div *ngIf="!access">you don't have access</div>
            <label class="badge badge-info p-2">
                toggle access
                <input type="checkbox" [(ngModel)]="access">
            </label>
        </div>
    </div>
</div>
<div class="col-4">
    <div class="card">
        <div class="card-header">ngStyle</div>
        <div class="card-body" [ngStyle]="{'font-size': style.size, 'color': style.color}">
            New style of this text!
        </div>
    </div>
</div>
<div class="col-4">
    <div class="card">
        <div class="card-header">ngClass</div>
        <div class="card-body">
            <div [ngClass]="myClass">my class name</div>
            <div [ngClass]="{'text-success':access}">text-success class when access</div>
            <div [class.text-danger]="access">text-danger class when access</div>
        </div>
    </div>
</div>
</div>

<div class="row">
    <div class="col-4">
    <div class="card">
        <div class="card-header">*ngFor</div>
        <ul class="list-unstyled card-body">
            <li *ngFor="let item of items; let idx=index">{{idx + 1}}. {{item}}</li>
        </ul>
    </div>
    </div>
    <div class="col-4">
            <div class="card">
        <div class="card-header">myColor - custom directive</div>
        <div class="card-body" [myColor]="'yellow'">
            hover your mouse
        </div>
    </div>
    </div>
    <div class="col-4">
            <div class="card">
        <div class="card-header">ngSwitch</div>
        <div class="card-body" [ngSwitch]="tik">
            <h1 *ngSwitchCase="1">the biggest {{tik}}</h1>
            <h2 *ngSwitchCase="2">very big {{tik}}</h2>
            <h3 *ngSwitchCase="3">quite big {{tik}}</h3>
            <h4 *ngSwitchCase="4">medium {{tik}}</h4>
            <h5 *ngSwitchCase="5">small {{tik}}</h5>
            <h6 *ngSwitchCase="6">very small {{tik}}</h6>
            <p *ngSwitchDefault>default {{tik}}</p>
        </div>
    </div>
    </div>
</div>
    `
})

export class DirectivesNgExamples {
    access: boolean = true;
    tik: number;
    style = {
        size: "33px",
        color: "coral"
    };
    myClass: string = "text-danger";
    items: Array<string> = ['Joe', 'Mike', 'Kate'];

    constructor() {

        interval(1000)
            .pipe(
                map(val => {
                    return val % 6 + 1;
                })
            )

            .subscribe(val => this.tik = val);
    }
}
