import { Component } from "@angular/core";
import { API } from "../../shared/api-end-points";

@Component({
    templateUrl: "./dom-modyfication.html"
})
export class DomModyficationExample {
    userName: string = "Jeo";
    access: boolean = false;
    editable: boolean = true;
    tagContent: string = `<h5>template</h5><img width="50" src='${API.IMAGE_END_POINT}'>`;
}
