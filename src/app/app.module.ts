import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { routingModule } from './app.routing';
import { ComponentExample } from './examples/component';
import { DirectivesNgExamples } from './examples/directives';
import { UserEventsExamples } from './examples/user-events';
import { DomModyficationExample } from './examples/dom-modyfication';
import { ServiceExample } from './examples/service';
import { PipesExample } from './examples/pipes';
import { HttpExample } from './examples/http';
import { FormModelDrivenExample } from './examples/form-model-driven';
import { RoutesExample } from './examples/routes';
import { UploadComponentExample } from './examples/upload';
import { OnInitExample } from './examples/component-lifecycle-hooks/onInit_onDestroy-example';
import { OnChangesExample } from './examples/component-lifecycle-hooks/onChanges-example';
import { DoCheckIterableExample } from './examples/component-lifecycle-hooks/doCheck-iterable-example';
import { AfterViewInitExample } from './examples/component-lifecycle-hooks/viewInit_contentInit-example';
import { Guard } from './examples/routes/services/guard';
import { HomeComponent } from './examples/routes/components/home-component';
import { AboutComponent } from './examples/routes/components/about-component';
import { ProductsComponent } from './examples/routes/components/products-component';
import { FirstProductComponent } from './examples/routes/components/products/first.component';
import { SecondProductComponent } from './examples/routes/components/products/second.component';
import { ProtectedComponent } from './examples/routes/components/protected-component';
import { ByIdProductComponent } from './examples/routes/components/products/byId.component';
import { ImgThumbnailComponent } from './examples/component/image-thumbnail.component';
import { TranslatePipe } from './examples/pipes/translate-pipe';
import { LimiterPipe } from './examples/pipes/limiter-pipe';
import { FormWithFormGroup } from './examples/form-model-driven/form-model-driven';
import { LoginComponent } from './examples/routes/components/login-component';
import { FileUpload } from './examples/upload/file-upload';
import { InitComponent } from './init.component';
import { LifeCycleComponent } from './examples/component-lifecycle-hooks';
import { FormTemplateDriven } from './examples/form-template-driven/form-template-driven';
import { FormTemplateDrivenExample } from './examples/form-template-driven';
import { AdultValidator } from './examples/form-template-driven/adult-validator.directive';
import { MyColorDirective } from './examples/directives/my-color.directive';
import { DoCheckObjectExample } from './examples/component-lifecycle-hooks/doCheck-object-example';
import { SizerComponent } from './examples/component/sizer.component';
import { SearchPipe } from "./examples/pipes/search.pipe";
import { HttpClientModule } from "@angular/common/http";
import { ToggleComponent } from './examples/component-lifecycle-hooks/components/toggle.component';
import { OnChangeComponent } from './examples/component-lifecycle-hooks/components/on-change.component';
import { AftersComponent } from './examples/component-lifecycle-hooks/components/afters.component';
import { DoCheckList } from './examples/component-lifecycle-hooks/components/do-check-list.component';
import { DoCheckObjectComponent } from './examples/component-lifecycle-hooks/components/do-check-object.component';
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

@NgModule({
    imports: [
        BrowserModule,
        HttpClientModule,
        ReactiveFormsModule, FormsModule,
        routingModule
    ],
    declarations: [
        AppComponent,
        InitComponent,
        ImgThumbnailComponent,
        TranslatePipe, LimiterPipe, SearchPipe,
        ComponentExample, DirectivesNgExamples, UserEventsExamples, DomModyficationExample, ServiceExample,
        PipesExample, HttpExample, FormModelDrivenExample, FormTemplateDriven, FormTemplateDrivenExample,
        AdultValidator,
        FormWithFormGroup, LoginComponent, FileUpload,
        ToggleComponent,
        OnChangeComponent,
        DoCheckList, DoCheckObjectComponent,
        AftersComponent,
        RoutesExample, HomeComponent, AboutComponent, ProductsComponent,
        FirstProductComponent, SecondProductComponent, ByIdProductComponent, ProtectedComponent,
        UploadComponentExample,
        OnInitExample, OnChangesExample, DoCheckIterableExample, DoCheckObjectExample,
        AfterViewInitExample,
        LifeCycleComponent,
        MyColorDirective, SizerComponent
    ],
    providers: [
        Guard
    ],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
